import pyproj, math, numpy as np

class N320(object):
    '''Class for helping convert between latitudes / longitudes and 
    grid points on an N320 reduced Gaussian grid.
    See https://confluence.ecmwf.int/display/UDOC/N320'''

    def __init__(self, n320_dir):
        # consts
        self.NUM_N320_PTS = 542080

        # input arrays
        self.lats = np.load(n320_dir + 'latitudes.npy') # the latitudes of the N320 grid
        self.lons = np.load(n320_dir + 'pts_per_latitude.npy') # num of evenly spaced longitudes on each of those latitudes

        # useful arrays:
        self.i_lons_end = np.cumsum(self.lons) # last index (exclusive) of the ith latitude
        self.i_lons_start = np.zeros(self.lons.shape, dtype='int')
        self.i_lons_start[1:] = self.i_lons_end[:-1] # first index (inclusive) of the ith latitude

        # for distances
        self.geod = pyproj.Geod(ellps='WGS84')

    def latlon_to_n320(self, lat, lon):
        '''convert a lat + lon the closest n320 index'''
    
        i_lat = np.argmin(np.abs(self.lats - lat)) # index into lats of closest latitude
        i_lon_start = self.i_lons_start[i_lat] # index into flat n320 array to get to start of said latitude
            
        num_divs = self.lons[i_lat] # split up 360 degs of longitude by number of divisions
        i_lon = int(round(lon / 360.0 * num_divs, 0)) % num_divs # index of closest one
        # print(num_divs, i_lon)
        
        return i_lon_start + i_lon


    def n320_to_latlon(self, i_n320):
        '''convert a n320 index to lat + lon'''
        
        i_lat = np.sum(i_n320 >= self.i_lons_end)
        
        lat = self.lats[i_lat]
        i_lon_start = self.i_lons_start[i_lat]
        
        i_lon = i_n320 - i_lon_start
        num_divs = self.lons[i_lat]
        lon = (i_lon * 360.0 / num_divs) % 360
        
        return lat, lon
        # lat is -90 to 90, lon is 0 to 360

    def __dist(self, lat1, lon1, lat2, lon2):
        '''distance in m between two points'''
        dist = self.geod.inv(lon1, lat1, lon2, lat2)[2]
        return max(dist, 0.0000000001) # to avoid divide by 0 errors

    def latlon_to_n320_interpolated(self, lat, lon):
        '''find the 4 closest N320 grid points in the NW, NE, SW, SE directions
        assign each a weight to use for interpolation (inverse distance to original point)
        this is not proper bilinear quadrilateral interpolation, but it's pretty good'''
        
        # bound lat within range
        if lat > self.lats[0]:
            lat = self.lats[0]
        elif lat < self.lats[-1]:
            lat = self.lats[-1]
        
        # get the two closest latitudes
        i_lat1, i_lat2 = np.argpartition(np.abs(self.lats - lat), 2)[:2] 
        lat1 = self.lats[i_lat1]
        i_lon_start1 = self.i_lons_start[i_lat1]
        lat2 = self.lats[i_lat2]
        i_lon_start2 = self.i_lons_start[i_lat2]
        
        # get two closest points on one latitude
        num_divs1 = self.lons[i_lat1]
        i_lon1A = math.floor(lon / 360.0 * num_divs1) % num_divs1
        i_lon1B = math.ceil(lon / 360.0 * num_divs1) % num_divs1
        if i_lon1A == i_lon1B:
            i_lon1B = (i_lon1B + 1) % num_divs1
        lon1A = i_lon1A * 360.0 / num_divs1
        lon1B = i_lon1B * 360.0 / num_divs1
        
        # get two closest points on other latitude
        num_divs2 = self.lons[i_lat2]
        i_lon2A = math.floor(lon / 360.0 * num_divs2) % num_divs2
        i_lon2B = math.ceil(lon / 360.0 * num_divs2) % num_divs2
        if i_lon2A == i_lon2B:
            i_lon2B = (i_lon2B + 1) % num_divs2
        lon2A = i_lon2A * 360.0 / num_divs2
        lon2B = i_lon2B * 360.0 / num_divs2
        
        results = {
                i_lon_start1 + i_lon1A: 1 / self.__dist(lat, lon, lat1, lon1A),
                i_lon_start1 + i_lon1B: 1 / self.__dist(lat, lon, lat1, lon1B),
                i_lon_start2 + i_lon2A: 1 / self.__dist(lat, lon, lat2, lon2A),
                i_lon_start2 + i_lon2B: 1 / self.__dist(lat, lon, lat2, lon2B),
        }
        scale = sum(results.values())
        results = {k: v/scale for k,v in results.items()}
        
        return results
