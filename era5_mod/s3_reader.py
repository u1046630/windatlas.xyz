
import numpy as np, pandas as pd
import boto3, os, io
from datetime import datetime, timedelta

from .n320 import N320

class S3Reader(object):
    '''class for reading ERA5 data that has been stored in a Amazon S3 bucket'''

    def __init__(self, data_dir, config, dir_type='s3'):
        assert(dir_type == 's3') # local storage not implemented
        self.data_dir = data_dir
        self.dir_type = dir_type
        self.config = config
        n320_dir = os.path.join(os.path.dirname(__file__), 'data/n320/')
        self.n320 = N320(n320_dir)

    def __load_array(self, path, i_n320):
        '''path to npz file that stores the particular n320 grid point data'''
        key = f'{path}{i_n320}.npy'
        try:
            npy = io.BytesIO(self.data_dir.Object(key).get()['Body'].read())
        except:
            raise Exception('Data does not exist for this location')
        return np.load(npy)

    def __df_format(date_from, date_to, **kwargs):
        time = np.arange(date_from, date_to, timedelta(hours=1))
        df = pd.DataFrame(kwargs, index=time)
        df.index.name = 'datetime'
        return df


    def __read_data(self, date_from, date_to, height, i_n320):
        # this function does the vertical interpolation
        
        # date_from and date_to represent the REQUESTED period
        # date1 and date2 represent the period STORED in a particular directory
        # dateA and dateB represent the period NEEDED from a particular directory
        # all of them are inclusive, exclusive respectively
            
        periods = {} # similar format as config
        temp_date = date_from
        while temp_date < date_to:
            for path, (date1,date2) in self.config.items():
                if temp_date >= date1 and temp_date < date2:
                    periods[path] = (temp_date, min(date2, date_to))
                    temp_date = date2
                    break
            else:
                raise Exception(f'[!] no data avaliable (date={temp_date})')
            
        hour = timedelta(hours=1)
        a = np.full((date_to - date_from) // hour, 0.0, dtype='float16')
        b = np.full((date_to - date_from) // hour, 0.0, dtype='float16')
        
        for path, (dateA,dateB) in periods.items():
            date1 = self.config[path][0] # start of STORED downloaded
            
            nhrs = (dateB - dateA) // hour
            i_src = (dateA - date1) // hour
            i_dst = (dateA - date_from) // hour

            array_a = self.__load_array(path + 'a/', i_n320)
            a[i_dst:(i_dst+nhrs)] = array_a[i_src:(i_src+nhrs)]

            array_b = self.__load_array(path + 'b/', i_n320)
            b[i_dst:(i_dst+nhrs)] = array_b[i_src:(i_src+nhrs)]
 
        return a + b * np.log(height)


    def wind_speed(self, date_from, date_to, height, location):
        # location can be integer (n320 index) or 2-tuple (lat,lon)
        # height is in meters
        # this function does horizontal interpolation

        if isinstance(location, int):
            i_n320 = location # location is n320 index (no interpolation)
            return self.__read_data(date_from, date_to, height, i_n320)

        elif isinstance(location, tuple):
            lat, lon = location # location is lat,lon (use inverse distance interpolation)
            weights = self.n320.latlon_to_n320_interpolated(lat, lon)

            hour = timedelta(hours=1)
            data = np.zeros((date_to - date_from) // hour, dtype='float16')
            for i_n320, weight in weights.items():
                data += self.__read_data(date_from, date_to, height, i_n320)*weight
            return data

        else:
            raise Exception('[!] location must be int or tuple')
        


