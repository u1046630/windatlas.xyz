from django.views.generic import ListView, TemplateView
from django.conf import settings

import os

class MyListView(ListView):
    '''A base class for ListViews that filters objects by
    a provided set of GET parameters'''

    def get_queryset(self):
        objs = self.model.objects
        if hasattr(self, 'filter_params'):
            filters = {x: self.request.GET[x] for x in self.filter_params if x in self.request.GET}
            objs = objs.filter(**filters)
        if hasattr(self, 'order_param'):
            objs = objs.order_by(self.order_param)
        return objs


class MyMarkdownView(TemplateView):
    '''A base class that passes the contents of the file "self.markdown_name"
    to be rendered as markdown in "self.template_name"'''
    template_name = 'farms/markdown.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fname = os.path.join(settings.BASE_DIR, self.markdown_name)
        with open(fname, 'r') as f:
            context['markdown'] = f.read()
        return context