
### API Documentation

.

#### Required parameters

The parameters required to get raw ERA5 wind speeds are:

- `lat`: Latitude

- `lon`: Longitude

- `date_from` Inclusive start date of requested data in `YYYY-MM-DD` format.

- `date_to`: Inclusive end date of requested data in `YYYY-MM-DD` format. 

- `height`: Hub height in meters.

.

#### Convert wind speed to capacity factors

To convert wind speeds into capacity factors, use the `turbine` parameter:

- `turbine`: The turbine power curve to use, for example: `turbine=Vestas V29-225`. The list of avaliable turbine power curves is available [here](/turbines/). To make the capacity factors more accurate, the power curve will be adjusted using a `wakeloss` parameter and a `sigma` parameter. These will be set to the recommended values by default, but can be set manually.

.

#### Additional parameters

- `mean`: Used to average wind speed and capacity factor values over periods longer than an hour. Can be set to `day`, `month`, or `year`.

- `format`: Sets the returned file format. Can be set to `api` (default) or `csv`.

- `wakeloss`: The wind speeds are reduced by this value in m/s. The default value (recommeneded) is 0.71m/s (`wakeloss=0.71`). To stop the wakeloss parameter being applied, set it to zero (`wakeloss=0`).

- `sigma`: The power curve is Guassian smoothed with this value of sigma in m/s to account for the variations in wind speed within an hour and between individual turbines of a wind farm. The default value (recommended) is 1.17m/s (`sigma=1.17`). To stop this parameter being applied, set it to zero (`sigma=0`).

.

#### Global Wind Atlas adjustment (hidden)

This is a hidden API feature, wind speeds can be adjusted to match statistics from the [Global Wind Atlas](https://globalwindatlas.info/) (GWA). To use this feature, wind statistics for your location of interest have to be imported. See previously imported GWA statistics or import your own [here](/gwa-stats/). Then use the following parameter in the API:

- `gwa_stat`: The name of the object holding the Global Wind Atlas statistics. Example: `gwa_stat=Pocona`. Note that the API will not check that the coordinates the GWA Stat object matches the coordinates of the API request. 

.

Behind the scenes the API is adjusting the wind speeds to match the Global Wind Atlas "Hourly vs Monthly (cross table)" plot, then adjusting the average wind speed to match the value from the "Mean Wind Speed" plot.

.

### Avaliable data

Hourly wind speed data is avaliable for the period 1980 to 2019 (inclusive) for most offshore locations within 200km of a coastline.

Onshore wind speed data is also available for Bolivia and Chile, although these location are not highlighted on the map.

.

### Examples

Get the raw hourly ERA5 wind speeds at location off the coast of UK (London Array wind farm) for all of January 2019 at a hub height of 100m.

`/api/wind/?lat=51.626&lon=1.496&height=100&date_from=2019-01-01&date_to=2019-01-31`

Get the wind speeds for a location in Bolivia, for all of 2010, adjusted to match the Global Wind Atlas statistics. Also calculate the capacity factor for a Vestas V29-225 turbine. Average the results over day periods.

`/api/wind/?lat=-15.968&lon=-68.665&height=100&date_from=2010-01-01&date_to=2011-12-31&gwa_stat=bolivia1&turbine=Vestas%20V29-225&mean=day`