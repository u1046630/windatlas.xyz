### Data Acknowledgements
.

#### Wind speeds: ERA5

ERA5 is a reanalysis, the output of global weather modelling, by ECMWF (European Centre for Medium-Range Weather Forecasts). It contains hourly estimates of wind speed at a spacial resolution of 31km from 1979 to present that have previously proven valuable to wind farm research. This website provides easy access to 40 years (1980-2019) of offshore wind speeds at any wind turbine hub height.

.

#### Generation data: ENTSO-E

Power generation of offshore wind farms were downloaded from the ENTSO-E Transparency Platform in the form of hourly generation traces. The generation data was used to test the accuracy of our generation predicitions based on ERA5 wind speeds. ENTSO-E (European Network of Transmission System Operators for Electricity) hosts any transparency data made available by the the 42 constituent transmission system operators, and is the largest source of offshore wind generation data. The 57 hourly generation traces obtained each represent an individual offshore wind farm, or part thereof (44 from the UK, 7 in Belgium, 6 in Denmark). 

.

#### Wind farm metadata: The Wind Power

The name, location (latitude and longitude), turbine model, hub height, and commission and decommission date was acquired for European offshore wind farms from [thewindpower.net](http://thewindpower.net) with manual addition and corrections. The power curves of the various turbine models was also sourced from [thewindpower.net](http://thewindpower.net). Where a power curve was not available, the power curve from an alternate turbine, with a similar power per swept area, was used. 

.

