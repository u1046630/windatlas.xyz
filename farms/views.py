from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.conf import settings
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.views.generic import TemplateView, View
from django.contrib.staticfiles.storage import staticfiles_storage
from .models import Farm, Trace, GwaStat, PowerCurve
from .forms import GwaStatForm, APIForm
from .base_views import MyListView, MyMarkdownView

from jsonview.views import JsonView

import os, time, math
import numpy as np, pandas as pd
from datetime import datetime, timedelta, date
from plotly.offline import plot
import plotly.graph_objs as go

def read_timeseries(file):
    return pd.read_csv(file).set_index('datetime')

class InfoPageView(MyMarkdownView):
    markdown_name = 'farms/static/farms/info.md'

class ApiDocsView(MyMarkdownView):
    markdown_name = 'farms/static/farms/api-docs.md'

class AboutPageView(TemplateView):
    template_name = 'farms/about.html'

class MapView(TemplateView):
    template_name = 'farms/map.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['version'] = str(int(time.time()))
        context['url_icon'] = staticfiles_storage.url('farms/farm-icon-onshore.png')
        return context
        
class FarmsJsonView(JsonView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        farms = Farm.objects.all()
        farms = [{
            'name': x.name, 
            'lat': x.lat, 
            'lon': x.lon,
            'link': reverse('farms-farm') + f'?name={x.name}',
        } for x in farms]
        context['farms'] = farms
        return context

class CoordView(TemplateView):
    template_name = 'farms/coord.html'
    form_class = APIForm

    def get(self, request, *args, **kwargs):
        initial = {}
        for field in ['lat', 'lon', 'year', 'height', 'period']:
            if field in self.request.GET:
                initial[field] = self.request.GET[field]
        form = self.form_class(initial=initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):

        form = self.form_class(request.POST)
        if form.is_valid():
            query_strs = []

            # lat and lon
            lat = form.cleaned_data['lat']
            lon = form.cleaned_data['lon']
            query_strs.append(f'lat={lat}&lon={lon}')

            # height
            height = form.cleaned_data['height']
            query_strs.append(f'height={height}')

            # dates
            if form.cleaned_data['custom_date_range']:
                date_from = form.cleaned_data['date_from']
                date_to = form.cleaned_data['date_to']
            else:
                year = form.cleaned_data['year']
                date_from = date(year, 1, 1)
                date_to = date(year, 12, 31)
            date_from = date_from.strftime('%Y-%m-%d')
            date_to = date_to.strftime('%Y-%m-%d')
            query_strs.append(f'date_from={date_from}&date_to={date_to}')
            
            # turbine
            if form.cleaned_data['calculate_capacity_factors']:
                turbine = form.cleaned_data['turbine'].name
                query_strs.append(f'turbine={turbine}')

            # period
            period = form.cleaned_data['period']
            if period == 'day' or period == 'month':
                query_strs.append(f'&mean={period}')

            # download
            if 'download' in request.POST:
                query_strs.append(f'&format=csv')

            return HttpResponseRedirect(reverse('api-wind') + '?' + '&'.join(query_strs))

        return render(request, self.template_name, {'form': form})


class FarmView(TemplateView):
    template_name = 'farms/farm.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        name = self.request.GET.get('name', None)
        # if None: HTTP redirect to url 'farms-list'
        farm = Farm.objects.get(name=name)
        context['obj'] = farm
        context['traces'] = farm.trace_set.all()
        #context['power_curve'] = PowerCurve.objects.get(name=farm.power_curve)

        return context

class FarmsView(MyListView):
    model = Farm
    template_name = 'farms/farms.html'
    filter_params = ('country', 'type')
    order_param = 'name'


class TraceView(TemplateView):
    template_name = 'farms/trace.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        name = self.request.GET.get('name', None)
        trace = Trace.objects.get(name=name)
        context['obj'] = trace

        era5_result = trace.resultera5_set.last()
        ninja_result = trace.resultninja_set.last()
        context['era5_result'] = era5_result
        context['ninja_result'] = ninja_result

        hour = timedelta(hours=1)
        date_from = datetime.strftime(max(trace.date_start, datetime(1980, 1, 1)), '%Y-%m-%d')
        date_to = datetime.strftime(min(trace.date_end - hour, datetime(2019, 12, 31, 11)), '%Y-%m-%d')
        context['api_request'] = f'?lat={trace.farm.lat}&lon={trace.farm.lon}&date_from={date_from}&date_to={date_to}&height={trace.farm.height}&turbine={trace.farm.power_curve}&wakeloss={era5_result.era5_cf.wakeloss}&sigma={era5_result.era5_cf.sigma}'

        return context


class GwaStatsView(MyListView):
    model = GwaStat
    template_name = 'farms/gwa_stats.html'
    filter_params = ('country')
    order_param = 'name'


class GwaStatView(TemplateView):
    template_name = 'farms/gwa_stat.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        name = self.request.GET.get('name', None)
        obj = GwaStat.objects.get(name=name)
        context['obj'] = obj

        return context


class TurbinesView(MyListView):
    model = PowerCurve
    template_name = 'farms/turbines.html'
    filter_params = tuple()
    order_param = 'name'


class TurbineView(TemplateView):
    template_name = 'farms/turbine.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        name = self.request.GET.get('name', None)
        obj = PowerCurve.objects.get(name=name)
        context['obj'] = obj
        context['power_per_area'] = 1000 * obj.power / math.pi / (obj.diameter/2)**2

        df = pd.read_csv(obj.data)

        fig1 = go.Figure(go.Scatter(x=df.wind, y=df.power))
        context['plot_power_curve'] = plot(fig1, output_type='div', include_plotlyjs=False)

        return context


class GwaStatFormView(View):
    model_class = GwaStat
    form_class = GwaStatForm
    template_name = 'farms/gwa_stat_form.html'

    def get(self, request, *args, **kwargs):
        if 'name' in self.request.GET:
            obj = self.model_class.objects.get(name=self.request.GET['name'])
            form = self.form_class(instance=obj)
        else:
            form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        name = request.POST['name']
        obj = self.model_class.objects.filter(name=name).first()
        form = self.form_class(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            new_obj = form.save()
            if obj is None:
                messages.success(request, 'New GWA Stat created!')
            else:
                messages.success(request, 'GWA Stat edited!')

            return HttpResponseRedirect(reverse('farms-gwa-stat') + f'?name={new_obj.name}')

        return render(request, self.template_name, {'form': form})

class SupplementaryView(TemplateView):
    template_name = 'farms/supplementary_data.html'