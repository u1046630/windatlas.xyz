import numpy as np, pandas as pd
import matplotlib.pyplot as plt

def plot_weibulls(name, a_gwa, k_gwa, f_gwa, a_era, k_era, f_era):
    
    plt.figure(figsize=(15,6))
    plt.suptitle('Weibull distributions from GWC file\n' + name)
    width = 2*np.pi / 12 * 0.9 * 180/np.pi
    theta = np.linspace(0.0, 2*np.pi, 12, endpoint=False) * 180/np.pi
    
    ax = plt.subplot(131)#, projection='polar')
    ax.bar(theta, a_gwa, width=width)
    ax.bar(theta, a_era, width=width*0.5)
    plt.ylabel('Weibull A parameter (m/s)')
    plt.xlabel('Direction (degrees)')
    plt.legend(['GWA', 'ERA5'])
    
    ax = plt.subplot(132)#, projection='polar')
    ax.bar(theta, k_gwa, width=width)
    ax.bar(theta, k_era, width=width*0.5)
    plt.ylabel('Weibull K parameter')
    plt.xlabel('Direction (degrees)')
    plt.legend(['GWA', 'ERA5'])
    
    ax = plt.subplot(133)#, projection='polar')
    ax.bar(theta, f_gwa, width=width)
    ax.bar(theta, f_era, width=width*0.5)
    plt.ylabel('Frequency (%)')
    plt.xlabel('Direction (degrees)')
    plt.legend(['GWA', 'ERA5'])

def plot_temporals(name, hour_gwa, month_gwa, year_gwa, hour_era, month_era, year_era):
    
    plt.figure(figsize=(15,10))
    plt.suptitle('temporal plots\n' + name)

    ax = plt.subplot(311)
    ax.plot(hour_gwa[1], hour_gwa[0], marker='o')
    ax.plot(hour_era[1], hour_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('hour')
    plt.ylabel('wind speed index')

    ax = plt.subplot(312)
    ax.plot(month_gwa[1], month_gwa[0], marker='o')
    ax.plot(month_era[1], month_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('month')
    plt.ylabel('wind speed index')

    ax = plt.subplot(313)
    ax.plot(year_gwa[1], year_gwa[0], marker='o')
    ax.plot(year_era[1], year_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('year')
    plt.ylabel('wind speed index')
    
def plot_crosstables(name, crosstable_gwa, crosstable_era):
    
    x_axis, y_axis = np.mgrid[:12+1,:24+1]

    fig = plt.figure(figsize=(15, 6))
    plt.suptitle('cross table\n' + name)
    
    ax = plt.subplot(121)
    pc = ax.pcolormesh(x_axis, y_axis, crosstable_gwa)
    plt.title('GWA')
    plt.xlabel('month')
    plt.ylabel('hour')
    bar = fig.colorbar(pc)
    bar.set_label('wind speed index')
    
    ax = plt.subplot(122)
    pc = ax.pcolormesh(x_axis, y_axis, crosstable_era)
    plt.title('ERA5')
    plt.xlabel('month')
    plt.ylabel('hour')
    bar = fig.colorbar(pc)
    bar.set_label('wind speed index')
    
