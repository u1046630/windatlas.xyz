
# Local Deployment Guide

How to get this django project running on your local machine.

# pyenv and poetry

pyenv and poetry are requirements of this project, but also just useful tools to have when developing with python.

Install `pyenv`:
- `curl https://pyenv.run | bash`
- add the following to your `~/.basrc`:
```
# pyenv
export PATH="~/.pyenv/bin:$PATH"` to `~/.bashrc`
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```
- `source ~/.bashrc`

Install `python-3.8.2` with `pyenv`:
```
sudo apt update && sudo apt install -y make build-essential \
libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev \
wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev \
libffi-dev liblzma-dev python-openssl git

pyenv install 3.8.2
```

Install `poetry`:
- `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python`
- add `source $HOME/.poetry/env` to your `~/.bashrc`
- `source ~/.bashrc`

# install

Add the following to your `~/.bashrc`:
```
# django settings
export DJANGO_LOCATION='home'
export DJANGO_SECRET_KEY='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
```
Create your own secret key of random characters.

Then do the following:
```
source ~/.bashrc
git clone https://gitlab.anu.edu.au/u1046630/windatlas.xyz
cd windatlas.xyz
poetry install # install all python packages
./manage migrate
./manage runserver
```

Browse to `localhost:8000` to see the website. 

It'll have no data (plots, turbines etc) yet many webpages assume there is data, so expect a few 500 internal service errors.

# import the relevant data

The website data can be imported two ways:

## 1. From scratch

Import data with the `insert_data.py` script. This imports the following from "drive1":
- wind turbines and their power curves
- renewables ninja wind speed traces
- wind farm data, and ENTSO-E generation trace data
- some global wind atlas stats
- ERA5 wind speed traces are downloaded from the S3 bucket

Plot files, and experiment results, still need to be generated:
```
./scripts/experiments/run.py ninja              # create ninja results
./scripts/experiments/run.py era5 0.71 1.17     # create era5 results
./scripts/experiments/run.py trace_plots        # create generation trace plots
```

## 2. From backup

Get the database backup and the media directory from the `windatlas.xyz - data` repo.

Make sure all migrations are applied then:
```
./manage.py flush
./manage.py loaddata database-backup.json
```

Unzip `media.zip` to `media/` at the root of this repo.