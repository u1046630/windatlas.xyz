from django.conf import settings
import boto3, era5_mod
from datetime import datetime

date = lambda x: datetime(x, 1, 1)
s3 = boto3.resource("s3")
bucket = s3.Bucket('windsite-files')
era5_data_dir = bucket
era5_config = {
    'era5/1980-1989/': (date(1980), date(1990)),
    'era5/1990-1999/': (date(1990), date(2000)),
    'era5/2000-2009/': (date(2000), date(2010)),
    'era5/2010-2019/': (date(2010), date(2020)),
}
era5_reader = era5_mod.S3Reader(era5_data_dir, era5_config)