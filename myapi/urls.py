from django.urls import include, path
from rest_framework import routers
from .views import WindDataView

#router = routers.DefaultRouter()
#router.register('wind-data', views.WindDataView)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('wind/', WindDataView.as_view(), name='api-wind'),
]