from django.conf import settings

from rest_framework.renderers import BrowsableAPIRenderer, BaseRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import APIException
from rest_framework.throttling import UserRateThrottle

from farms.models import PowerCurve, GwaStat
from .globals import era5_reader

import era5_mod, gwa_mod

import io, pandas as pd, numpy as np
from datetime import datetime, timedelta

from scipy import ndimage

### helper funcs ###

def adjust_power_curve(curve_x, curve_y, wakeloss, sigma):
    curve_x = curve_x + wakeloss
    curve_y = curve_y / curve_y.max()
    if sigma > 0:
        step = curve_x[1] - curve_x[0]
        sigma /= step
        curve_y = ndimage.filters.gaussian_filter1d(curve_y, sigma, mode='nearest')
    
    return curve_x, curve_y

### helper classes ###

class CsvRenderer(BaseRenderer):
    """
    Return as CSV file. View is response for providing a pandas dataframe.
    """
    media_type = 'application/csv'
    format = 'csv'
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if isinstance(data, tuple):
            metadata, dataframe = data
            buffer = io.StringIO()
            buffer.write(f'metadata: {metadata}\n\n')
            dataframe.to_csv(buffer)
            buffer.seek(0)
            return buffer.read()
        # else: most likely an APIException
        return data

class BurstRateThrottle(UserRateThrottle):
    scope = 'burst'

class SustainedThrottle(UserRateThrottle):
    scope = 'sustained'

#############
### views ###
#############

class WindDataView(APIView):
    throttle_classes = [BurstRateThrottle, SustainedThrottle]

    renderer_classes = [CsvRenderer, BrowsableAPIRenderer]
    http_method_names = ['get', 'head'] # don't allow OPTIONS

    def get(self, request, format=None):

        # get basic parameters sorted
        missing = ['lat', 'lon', 'date_from', 'date_to', 'height']
        missing = [x for x in missing if x not in request.GET]
        if len(missing) > 0:
            raise APIException('Missing parameters: ' + ', '.join(missing))
        try:
            lat = float(request.GET['lat'])
            lon = float(request.GET['lon'])
            date_from = datetime.strptime(request.GET['date_from'], '%Y-%m-%d')
            date_to = datetime.strptime(request.GET['date_to'], '%Y-%m-%d') + timedelta(days=1)
            height = float(request.GET['height'])
        except ValueError:
            raise APIException('Bad parameter values')

        # get ERA5 data
        try:
            wind = era5_reader.wind_speed(date_from, date_to, height, (lat, lon))
            time_vect = np.arange(date_from, date_to, timedelta(hours=1))
        except Exception as e:
            raise APIException(e)

        # optional: adjust wind speed to match GlobalWindAtlas statistic
        if 'gwa_stat' in request.GET:
            gwa_stat = GwaStat.objects.filter(name=request.GET['gwa_stat']).first()
            if gwa_stat == None:
                raise APIException('GWA stat not found ({})'.format(request.GET['gwa_stat']))
            gwa_adjuster = gwa_mod.Adjuster('scalar', wind, time_vect, gwa_stat.gwc_file.path, gwa_stat.plots_file.path, gwa_stat.roughness, gwa_stat.height)
            try:
                gwa_adjuster.auto_adjust()
            except Exception as e:
                raise APIException(e)
            wind = gwa_adjuster.wind

        results = pd.DataFrame({'datetime': time_vect, 'wind_speed': wind}).set_index('datetime')

        # optional calculate capacity factors from wind speed
        # (requires 'turbine' parameter, optional 'sigma' and 'wakeloss' parameters)
        if 'turbine' in request.GET:
            turbine = PowerCurve.objects.filter(name=request.GET['turbine']).first()
            if turbine == None:
                raise APIException('Turbine model not found ({})'.format(request.GET['turbine']))
            if turbine.data == None:
                raise APIException('Turbine model power curve not avaliable')
            df = pd.read_csv(turbine.data)
            curve_x = df['wind']
            curve_y = df['power']

            wakeloss = request.GET.get('wakeloss', settings.API_DEFAULTS['wakeloss'])
            sigma = request.GET.get('sigma', settings.API_DEFAULTS['sigma'])

            try:
                wakeloss = float(wakeloss)
            except ValueError:
                raise APIException('Bad wakeloss parameter, must be a number')
            try:
                sigma = float(sigma)
            except:
                raise APIException('Bad sigma parameter, must be a number')
            if sigma < 0:
                raise APIException('Sigma can\'t be negative')

            curve_x, curve_y = adjust_power_curve(curve_x, curve_y, wakeloss, sigma)
            results['capacity_factor'] = np.interp(results.wind_speed, curve_x, curve_y)

        # optionally average longer time periods (instead of hourly)
        # parameter 'mean' must be 'day', 'month', or 'year'
        if 'mean' in request.GET:
            freq = {'day': 'D', 'month': 'M', 'year': 'Y'}.get(request.GET['mean'])
            if freq == None:
                raise APIException('"mean" parameter must be "day", "month" or "year"')
            results = results.groupby(pd.Grouper(freq=freq)).mean()

        mean_wind_speed = np.asarray(results.wind_speed).mean()
        if mean_wind_speed > (8.5+10)/2:
            wind_class = 'IEC I - High Wind'
        elif mean_wind_speed > (7.5+8.5)/2:
            wind_class = 'IEC II - Medium Wind'
        elif mean_wind_speed > (6+7.5)/2:
            wind_class = 'IEC III - Low Wind'
        else:
            wind_class = 'IEC IV - Very Low Wind'

        metadata = {'mean_wind_speed': f'{mean_wind_speed:.2f} ({wind_class})'}
        for k, v in dict(request.GET).items():
            metadata[k] = v[0]

        response = Response((metadata,results))
        if request.GET.get('format') == 'csv':
            response['Content-Disposition'] = 'attachment; filename="windatlas-xyz-data.csv"'
        return response