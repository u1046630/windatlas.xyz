
# windatlas.xyz

This repo contains all the code needed to deploy the website. It is a django project, to be deployed with nginx and gunicorn. 

A database backup (containing turbines, validation results, European wind farms etc.) can be found in the "windatlas.xyz data" repo.
This backup also includes the files referenced by the database objects.

The ERA5 wind speed data is hosted in a S3 bucket.

Combining these three (code + database backup + S3 bucket) gives the fully functional website.