
# Remote Deployment Guide

This explains how to deploy this website to run on an Amazon Web Services EC2 instance.

It assumes that you've got this django project working on your local machine first.

# AWS EC2

On AWS EC2:
- Launch t2.small Ubuntu-18.04 instance.
- Allow port 8000 to go through for initial testing

On local machine:
- Edit /etc/hosts to give the IP the name "windatlas"
- Edit ~/.ssh/config to give default username "ubuntu" and identity file
- SSH: "ssh windatlas"

On remote machine:
- Install pyenv, poetry, postgres, nginx. 
- `sudo apt install libpq-dev`, a package required for postgres
- git clone the repo
- `cd windatlas.xyz`
- `poetry install` to install required python packages

# Database

The remote django project uses PostGres database.

postgres and libpq-dev should installed system packages.
psycopg2 should be an installed python package.

On remote machine `sudo -u postgres psql` (create real password for postgres database):
```
CREATE DATABASE windatlas_db;
CREATE USER windatlas_user WITH PASSWORD 'xxxxxxxxxxxxxxxxxx';
ALTER ROLE windatlas_user SET client_encoding TO 'utf8';
ALTER ROLE windatlas_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE windatlas_user SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE windatlas_db TO windatlas_user;
\q
```

On remove machine:
- `./manage migrate`
If it complains that "django.db.utils.ProgrammingError: relation "farms_powercurve" does not exist"
then try commenting out "initial=PowerCurve.objects.get(name='Vestas V126-3450')," from `farms/forms.py`.
Or maybe there is a similar offending line somewhere. I don't understand why this happens.

# S3

The remote machine needs the credentials to access the S3 bucket which contains the ERA5 wind speed data.

The Amazon resource name (ARN) of the S3 bucket is `arn:aws:s3:::windsite-files`. 
It is owned by my own AWS account (Liam Hayes), so only I should have the credentials for it.

create `~/.aws/config` (something like this):
```
[default]
region = ap-southeast-2
```

create `~/.aws/credentials` (something like this, make some real credentials on AWS IAM):
```
[default]       
aws_access_key_id = LSKEJFLKJESLKJFLKEJJ
aws_secret_access_key = LKj34lknol2krlkjlij+lkjkjkeLLkwnLkngleks
```

# Gunicorn

Follow DigitalOcean tutorial starting from "Testing Gunicorn’s Ability to Serve the Project":
- `https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04`

This is what it should end up with (only major differnce is the inclusion of the environment variables in `gunicorn.service`):

`/etc/systemd/system/gunicorn.socket`:
```
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
```

Create `/etc/systemd/system/gunicorn.service` (put in real DJANGO_SECRET_KEY and DJANGO_POSTGRES_PASSWORD)
```
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/windatlas.xyz
ExecStart=/home/ubuntu/.cache/pypoetry/virtualenvs/windatlas-LMkbtO5w-py3.8/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          --env DJANGO_LOCATION='ec2' \
          --env DJANGO_SECRET_KEY='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' \
          --env DJANGO_POSTGRES_PASSWORD='xxxxxxxxxxxxxxxxxx' \
          windatlas.wsgi:application

[Install]
WantedBy=multi-user.target
```

The relevant commands are:
```
sudo systemctl daemon-reload                            # when .socket and .service files are updated on disk
sudo systemctl start gunicorn                           # start service
sudo systemctl status gunicorn                          # see if it's working
file /run/gunicorn.sock                                 # check there is a socket file here
curl --unix-socket /run/gunicorn.sock localhost         # test socket is working (returns HTML from django project)
sudo systemctl restart gunicorn                         # restart service, do this when .service file is updated, or django project is updated
```

# Nginx 

`sudo apt install nginx`

Following the same Digital Ocean tutorial

Create `/etc/nginx/sites-available/windatlas`:
```
server {
    listen 80;
    server_name windatlas.xyz;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/ubuntu/windatlas.xyz;
    }

    location /media/ {
        root /home/ubuntu/windatlas.xyz;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
```
Then `sudo nginx -t` (to test the config file) and `sudo systemctl restart nginx`.

The django application will only be displayed by nginx if browsing from `windatlas.xyz`. 
If you see a "welcome to nginx" where the webapp should be, check the `server_name` parameter matches the domain in your URL.

# Domain name

In AWS EC2, allocate an elastic IP to the instance.

In the domain name provider settings (godaddy), point the "windatlas.xyz" domain at the elastic IP address (find online tutorial).

# Migrate data

Move database and media files from local to remote.

This is a **dangerous** step!! Make sure both the local and remote machine have the same models and migrations, and all migrations
have been applied. Consider making a snapshot of the EC2 instance!

Move the database first (the bit that can go wrong):

On local machine:
```
mkdir datadumps/
./manage.py dumpdata --natural-primary --natural-foreign > datadumps/db_4Dec20.json
scp datadumps/db_4Dec20.json windatlas:~/data/db_4Dec20.json
```

On remote machine:
```
./manage flush
git pull
./manage.py migrate
./manage.py loaddata ~/datadumps/database.json
```

The remote server should now have data on it, but the database entries may refer to media files that 
don't yet exist. 

Now move the media files (on local machine):
- `rsync -rvv --delete media/ windatlas:/home/ubuntu/windatlas.xyz/media/` (~330MB).
- `r` for recursive, `vv` for very verbose, `delete` for delete files on remote that aren't on local
- For big transfers it'd be faster to make `media.zip` file, and copy that across instead.
