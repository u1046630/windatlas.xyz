#!/usr/bin/env python

# script for running "experiments", generating capacity factor predictions from 
# ERA5 and Ninja data that is already in the database, and comparing it to the read data

import os, sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "windatlas.settings")
import django
django.setup()

import src as experiments

if len(sys.argv) == 1:
    raise Exception(f'[!] usage: {sys.argv[0]} <experiment_name>')

exp_name = sys.argv[1].lower()

if exp_name.lower() == 'ninja':
    if len(sys.argv) != 2:
        raise Exception(f'[!] usage: {sys.argv[0]} {exp_name}')
    exp = experiments.Ninja()
    
elif exp_name == 'era5':
    if len(sys.argv) != 4:
        raise Exception(f'[!] usage: {sys.argv[0]} {exp_name} <wakeloss1,wakeloss2...> <sigma1,sigma2...>')
    wakelosses = [float(x) for x in sys.argv[2].split(',')]
    sigmas = [float(x) for x in sys.argv[3].split(',')]
    exp = experiments.TestParams(wakelosses, sigmas)

elif exp_name == 'trace_plots':
    if len(sys.argv) != 2:
        raise Exception(f'[!] usage: {sys.argv[0]} {exp_name}')
    exp = experiments.TracePlots()

elif exp_name == 'fit_dcurve':
    if len(sys.argv) != 2:
        raise Exception(f'[!] usage: {sys.argv[0]} {exp_name}')
    exp = experiments.FitDcurve()

elif exp_name == 'fit_timeseries':
    if len(sys.argv) != 2:
        raise Exception(f'[!] usage: {sys.argv[0]} {exp_name}')
    exp = experiments.FitTimeseries()

else:
    raise NotImplementedError()

results = exp.run()
exp.save()