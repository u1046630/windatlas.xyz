import os
from django.conf import settings

# where to start optimisations from
WAKELOSS0 = 0.71
SIGMA0 = 1.17 

# where to output experiment results .csv files
OUTPUT_DIR = os.path.join(settings.BASE_DIR, 'scripts/experiments/results/')