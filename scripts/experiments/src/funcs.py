import pandas as pd, numpy as np, io
from scipy import ndimage
from scipy import optimize
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline

from .config import WAKELOSS0, SIGMA0

from farms.models import SeriesEra5CF, SeriesNinjaCF, ResultEra5, ResultNinja
from django.core.files import File

def normalise_pcurve(pcurve):
    '''convert pcurve to a capacity factor curve'''
    pcurve.power = pcurve.power / pcurve.power.max()

def adjust_pcurve(pcurve, wakeloss, sigma):   
    '''adjust power curve with wakeloss and sigma parameters''' 
    pcurve = pcurve.copy()
    pcurve.wind = pcurve.wind + wakeloss
    if sigma > 0:
        step = pcurve.wind[1] - pcurve.wind[0]
        sigma /= step
        pcurve.power = ndimage.filters.gaussian_filter1d(pcurve.power, sigma, mode='nearest')
    return pcurve

def wind_to_cf(wind, pcurve):
    '''predict capacity factor from wind and power curve'''
    return np.interp(wind, pcurve.wind, pcurve.power)

def fit_timeseries(cf_trace, wind, pcurve):
    '''find wakeloss and sigma parameters to best predict capacity factor timeseries'''

    def func(wind, wakeloss, sigma):
        pcurve_adjusted = adjust_pcurve(pcurve, wakeloss, sigma)
        cf_predicted = wind_to_cf(wind, pcurve_adjusted)
        return cf_predicted

    params, pcov = optimize.curve_fit(func, wind, cf_trace, p0=(WAKELOSS0, SIGMA0))
    perr = np.sqrt(np.diag(pcov))
    if params[1] < 0:
        params[1] = 0
    return params, perr

def fit_dcurve(cf_trace, wind, pcurve):
    '''find wakeloss and sigma parameters to best predict capacity duration curve'''
    dcurve = np.array(cf_trace)
    dcurve.sort()

    def func(wind, wakeloss, sigma):
        pcurve_adjusted = adjust_pcurve(pcurve, wakeloss, sigma)
        cf_predicted = wind_to_cf(wind, pcurve_adjusted)
        cf_predicted.sort()
        return cf_predicted

    params, pcov = optimize.curve_fit(func, wind, dcurve, p0=(WAKELOSS0, SIGMA0))
    perr = np.sqrt(np.diag(pcov))
    if params[1] < 0:
        params[1] = 0
    return params, perr

def rmse(x1, x2):
    '''root mean squared error'''
    return np.sqrt(((x1-x2)**2).mean())

def read_timeseries_csv(file):
    '''csv file to pandas dataframe'''
    return pd.read_csv(file, parse_dates=['datetime']).set_index('datetime')

def read_pcurve(file):
    '''csv file to pandas dataframe'''
    pcurve = pd.read_csv(file)
    normalise_pcurve(pcurve)
    return pcurve

def plot_trace(data):
    #fig, ax1 = plt.subplots(1, 1, figsize=(15, 5))
    plt.rcParams.update({'font.size': 18})
    fig, ax1 = plt.subplots(1, 1, figsize=(30, 10))

    ax1.plot(data.index, data.power, linewidth=0.2, alpha=0.5)
    ax1.plot(data.index, data.power / data.cf_trace)
    ax1.set_xlabel('Time')
    ax1.set_ylabel('Power (MW)')
    ax1.set_title('Generation Trace')
    ax1.legend(['Generated Power', 'Inferred Capacity'])

    buf = io.BytesIO()
    fig.savefig(buf, bbox_inches='tight')
    plt.close(fig)
    return buf

def plot_prediction(data, col_name, rmse_hourly, rmse_daily, rmse_dcurve):

    data_daily = data.groupby(pd.Grouper(freq='D')).mean()

    #fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(15,5))
    plt.rcParams.update({'font.size': 18})

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(18,3.5))
    #fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(30,10))
    
    ax1.plot([0,1], [0,1], color='black', linewidth=0.5)
    ax1.scatter(data[col_name], data.cf_trace, alpha=0.5, s=0.2)
    ax1.set_xlabel('Predicted')
    ax1.set_ylabel('Actual')
    ax1.set_title(f'Hourly capacity factors\nRMSE={rmse_hourly:.3f}')
    
    ax2.plot([0,1], [0,1], color='black', linewidth=1)
    ax2.scatter(data_daily.cf_trace, data_daily[col_name], alpha=0.8, s=2.0)
    #ax2.set_xlabel('Actual')
    #ax2.set_ylabel('Predicted')
    ax2.set_title(f'Daily capacity factors\nRMSE={rmse_daily:.3f}')
    
    cf_trace = np.array(data.cf_trace)
    cf_trace[::-1].sort()
    cf_predicted = np.array(data[col_name])
    cf_predicted[::-1].sort()
    duration = np.linspace(0, 100, len(cf_trace))
    ax3.plot(duration, cf_trace, color='red', linewidth=3)
    ax3.plot(duration, cf_predicted, color='orange', linewidth=3)
    ax3.legend(['Actual','Predicted'])
    ax3.set_xlabel('Duration exceeded (%)')
    ax3.set_ylabel('Capacity factor')
    ax3.set_title(f'Duration curve of hourly capacity factors\nRMSE={rmse_dcurve:.3f}')
    
    buf = io.BytesIO()
    fig.savefig(buf, bbox_inches='tight')
    plt.close(fig)
    return buf

def save_trace_plot(trace, data):
    plot_buf = plot_trace(data)
    trace.plot_file = File(plot_buf, 'plot.png')
    trace.save()

#############
### ninja ###
#############

def calc_ninja_errors(data):
    '''return dictionary of various error metrics'''
    errors = {}
    errors['rmse_ninja_hourly'] = rmse(data.cf_trace, data.cf_ninja)
    data_daily = data.groupby(pd.Grouper(freq='D')).mean()
    errors['rmse_ninja_daily'] = rmse(data_daily.cf_trace, data_daily.cf_ninja)
    data_monthly = data.groupby(pd.Grouper(freq='M')).mean()
    errors['rmse_ninja_monthly'] = rmse(data_monthly.cf_trace, data_monthly.cf_ninja)
    cf_trace = np.sort(np.array(data.cf_trace))
    cf_ninja = np.sort(np.array(data.cf_ninja))
    errors['rmse_ninja_dcurve'] = rmse(cf_trace, cf_ninja)
    return errors

def save_ninja_result(trace, result, data, description='ninja result'):

    series_df = data[['cf_ninja']]
    buf = io.StringIO()
    series_df.to_csv(buf)
    cf_obj = SeriesNinjaCF(
        wind=trace.farm.ninja_wind,
        power_curve=trace.farm.power_curve,
        date_start=data.index.min(),
        date_end=data.index.max(),
        data=File(buf, trace.name + '.csv'),
    )
    cf_obj.save()

    plot_buf = plot_prediction(data, 'cf_ninja', result['rmse_ninja_hourly'], result['rmse_ninja_daily'], result['rmse_ninja_dcurve'])
    result_obj = ResultNinja(
        description=description,
        trace=trace,
        ninja_cf=cf_obj,
        rmse_hourly=result['rmse_ninja_hourly'],
        rmse_daily=result['rmse_ninja_daily'],
        rmse_dcurve=result['rmse_ninja_dcurve'],
        plot_file=File(plot_buf, 'plot.png'),
    )
    result_obj.save()

def ninja_gaussian_filter(x, X, sigma):
    n = X - x
    return np.exp(-n**2 / (2*sigma)) / np.sqrt(2 * np.pi * sigma)

def ninja_smooth_pcurve(pcurve):
    # as directed in supplementary material of Staffel paper
    # section 4.3
    
    spline = CubicSpline(pcurve['wind'], pcurve['power'])
    res = 0.01
    X = np.arange(0, 40, res) # pts where pcurve is to be defined
    Y = spline(X) # cubic spline of power curve to higher resolution
    Y_prime = np.zeros(Y.shape)
    N = len(X)
    assert(len(Y) == N)

    for i in range(N):
        x = X[i]
        sigma = 0.6 + 0.2*x # sigma as function of wind speed formula
        g_filter = ninja_gaussian_filter(x, X, sigma)
        Y_prime[i] = np.sum(Y * g_filter)*res
        
    return pd.DataFrame({'wind': X, 'power': Y_prime})
    
def ninja_wind_to_cf(wind, country, pcurve):
    '''predict capacity factor from wind and power curve, according to renewables ninja method'''
    
    # normalise pcurve:
    pcurve.power = pcurve.power / pcurve.power.max()
    # gaussian smooth pcurve:
    pcurve = ninja_smooth_pcurve(pcurve)
    # interpolate to get capacity factors
    return np.interp(wind, pcurve.wind, pcurve.power)

############
### era5 ###
############

def calc_era5_errors(data):
    '''return dictionary of various error metrics'''
    errors = {}
    errors['rmse_era5_hourly'] = rmse(data.cf_trace, data.cf_era5)
    data_daily = data.groupby(pd.Grouper(freq='D')).mean()
    errors['rmse_era5_daily'] = rmse(data_daily.cf_trace, data_daily.cf_era5)
    data_monthly = data.groupby(pd.Grouper(freq='M')).mean()
    errors['rmse_era5_monthly'] = rmse(data_monthly.cf_trace, data_monthly.cf_era5)
    cf_trace = np.sort(np.array(data.cf_trace))
    cf_era5 = np.sort(np.array(data.cf_era5))
    errors['rmse_era5_dcurve'] = rmse(cf_trace, cf_era5)
    return errors

def save_era5_result(trace, result, data, description='era5 result'):
        
    series_df = data[['cf_era5']]
    buf = io.StringIO()
    series_df.to_csv(buf)
    cf_obj = SeriesEra5CF(
        wind=trace.farm.era5_wind,
        wakeloss=result['wakeloss'],
        sigma=result['sigma'],
        power_curve=trace.farm.power_curve,
        date_start=data.index.min(),
        date_end=data.index.max(),
        data=File(buf, trace.name + '.csv'),
    )
    cf_obj.save()

    plot_buf = plot_prediction(data, 'cf_era5', result['rmse_era5_hourly'], result['rmse_era5_daily'], result['rmse_era5_dcurve'])
    result_obj = ResultEra5(
        description=description,
        trace=trace,
        era5_cf=cf_obj,
        rmse_hourly=result['rmse_era5_hourly'],
        rmse_daily=result['rmse_era5_daily'],
        rmse_dcurve=result['rmse_era5_dcurve'],
        plot_file=File(plot_buf, 'plot.png')
    )
    result_obj.save()


