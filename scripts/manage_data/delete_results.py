# delete 

from farms.models import SeriesEra5CF, ResultEra5, SeriesNinjaCF, ResultNinja, Trace
import shutil, os

print('[*] deleting SeriesEra5CF objects')
if os.path.isdir('media/SeriesEra5CF'):
    shutil.rmtree('media/SeriesEra5CF')
SeriesEra5CF.objects.all().delete()

print('[*] deleting ResultEra5 objects')
if os.path.isdir('media/ResultEra5'):
    shutil.rmtree('media/ResultEra5')
ResultEra5.objects.all().delete()

print('[*] deleting SeriesNinjaCF objects')
if os.path.isdir('media/SeriesNinjaCF'):
    shutil.rmtree('media/SeriesNinjaCF')
SeriesNinjaCF.objects.all().delete()

print('[*] deleting ResultNinja objects')
if os.path.isdir('media/ResultNinja'):
    shutil.rmtree('media/ResultNinja')
ResultNinja.objects.all().delete()

print('[*] deleting Ninja result objects...')
if os.path.isdir('media/SeriesNinjaCF'):
    shutil.rmtree('media/SeriesNinjaCF')
SeriesNinjaCF.objects.all().delete()

print('[*] deleting plots from Traces...')
if os.path.isdir('media/Trace/plot'):
    shutil.rmtree('media/Trace/plot')
for trace in Trace.objects.all():
    trace.plot_file = None
    trace.save()