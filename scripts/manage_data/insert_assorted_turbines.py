
import pandas as pd
import io

from django.core.files import File
from farms.models import PowerCurve

drive1 = '~/data/'
assorted_turbines_dir = drive1 + 'assorted-turbines/'


def insert():
    print('[*] inserting extra power curves... ')
    details = pd.read_csv(assorted_turbines_dir + 'details.csv').set_index('name')
    
    for i, row in details.iterrows():
        pcurve_data = pd.read_csv(assorted_turbines_dir + row.file).set_index('wind')
        buf = io.StringIO()
        pcurve_data.to_csv(buf)
        pcurve_obj = PowerCurve(
            name=row.name,
            power=row.power,
            diameter=row.diameter,
            source='thewindpower',
            data=File(buf, row.name + '.csv')
        )
        pcurve_obj.save()
        
        
        print(f'    {row.name} done')
    
insert()
